package main

import (
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/illfalcon/hse-course-project/cpu-microservices/english/internal/service"

	"github.com/gorilla/mux"
)

func main() {
	engService := service.New()

	r := mux.NewRouter()
	r.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		msg, err := ioutil.ReadAll(request.Body)
		if err != nil {
			writer.WriteHeader(500)
			writer.Write([]byte("error"))
		}
		resp, err := engService.Transform(msg)
		if err != nil {
			writer.WriteHeader(500)
			writer.Write([]byte("error"))
		}
		writer.WriteHeader(200)
		writer.Write(resp)
	}).Methods(http.MethodPost)
	log.Println("start")
	log.Fatal(http.ListenAndServe(":8080", r))
}

package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/illfalcon/hse-course-project/cpu-microservices/gateway/internal/rpc"

	"github.com/gorilla/mux"
	"gitlab.com/illfalcon/hse-course-project/cpu-microservices/gateway/internal/message"
	"gitlab.com/illfalcon/hse-course-project/cpu-microservices/gateway/internal/transformer"
)

func main() {
	rusClient := rpc.NewClient(5 * time.Second)
	numClient := rpc.NewClient(5 * time.Second)
	engClient := rpc.NewClient(5 * time.Second)

	rusService := transformer.New(rusClient, os.Getenv("RUS_URL"))
	numService := transformer.New(numClient, os.Getenv("NUM_URL"))
	engService := transformer.New(engClient, os.Getenv("ENG_URL"))

	msgService := message.New(rusService, engService, numService)
	r := mux.NewRouter()
	r.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		msg, err := ioutil.ReadAll(request.Body)
		if err != nil {
			writer.WriteHeader(500)
			writer.Write([]byte("error"))
		}
		writer.WriteHeader(200)
		writer.Write([]byte(msgService.Transform(string(msg))))
	}).Methods(http.MethodPost)
	log.Println("start")
	log.Fatal(http.ListenAndServe(":8080", r))
}

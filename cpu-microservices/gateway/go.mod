module gitlab.com/illfalcon/hse-course-project/cpu-microservices/gateway

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/pkg/errors v0.9.1
)

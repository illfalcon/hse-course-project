package encoding

import "strconv"

func IntToStringMap(m map[int]string) map[string]string {
	res := make(map[string]string)
	for k, v := range m {
		res[strconv.Itoa(k)] = v
	}
	return res
}

func StringToIntMap(m map[string]string) (map[int]string, error) {
	res := make(map[int]string)
	for k, v := range m {
		i, err := strconv.Atoi(k)
		if err != nil {
			return nil, err
		}
		res[i] = v
	}
	return res, nil
}

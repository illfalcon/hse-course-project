package message

import (
	"strings"
)

type transformerClient interface {
	Transform(map[int]string) map[int]string
}

func isRussian(s string) bool {
	for _, r := range s {
		if (r < 'а' || r > 'я') && (r < 'А' || r > 'Я') {
			return false
		}
	}
	return true
}

func isEnglish(s string) bool {
	for _, r := range s {
		if (r < 'a' || r > 'z') && (r < 'A' || r > 'Z') {
			return false
		}
	}
	return true
}

func isNumber(s string) bool {
	for _, r := range s {
		if r < '0' || r > '9' {
			return false
		}
	}
	return true
}

type Service struct {
	russian transformerClient
	english transformerClient
	numbers transformerClient
}

func New(russian, english, numbers transformerClient) *Service {
	return &Service{
		russian: russian,
		english: english,
		numbers: numbers,
	}
}

func (s *Service) Transform(msg string) string {
	russianWords := make(map[int]string)
	englishWords := make(map[int]string)
	numberWords := make(map[int]string)
	invalidWords := make(map[int]string)

	words := strings.Split(msg, " ")

	for i, word := range words {
		if isRussian(word) {
			russianWords[i] = word
			continue
		}
		if isEnglish(word) {
			englishWords[i] = word
			continue
		}
		if isNumber(word) {
			numberWords[i] = word
			continue
		}
		invalidWords[i] = word
	}

	rusChan := make(chan map[int]string)
	engChan := make(chan map[int]string)
	numChan := make(chan map[int]string)

	go func() {
		defer close(engChan)
		m := s.english.Transform(englishWords)
		engChan <- m
	}()
	go func() {
		defer close(rusChan)
		m := s.russian.Transform(russianWords)
		rusChan <- m
	}()
	go func() {
		defer close(numChan)
		m := s.numbers.Transform(numberWords)
		numChan <- m
	}()

	for k, v := range <-rusChan {
		words[k] = v
	}
	for k, v := range <-engChan {
		words[k] = v
	}
	for k, v := range <-numChan {
		words[k] = v
	}
	for k, _ := range invalidWords {
		words[k] = "ERROR"
	}
	return strings.Join(words, " ")
}

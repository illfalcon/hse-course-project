package service

import (
	"encoding/json"
	"strconv"
	"time"
)

type Service struct {
}

func New() *Service {
	return &Service{}
}

func (s *Service) Transform(data []byte) ([]byte, error) {
	var words map[string]string
	err := json.Unmarshal(data, &words)
	if err != nil {
		return nil, err
	}
	for k, v := range words {
		i, err := strconv.Atoi(v)
		if err != nil {
			words[k] = "ERROR"
			continue
		}
		newWord := strconv.Itoa(i * i)
		words[k] = newWord

		done := make(chan struct{})

		for i := 0; i < 2; i++ {
			go func() {
				for {
					select {
					case <-done:
						return
					default:
					}
				}
			}()
		}

		time.Sleep(200 * time.Millisecond)
		close(done)
	}

	res, err := json.Marshal(words)
	if err != nil {
		return nil, err
	}
	return res, nil
}

package main

import (
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/illfalcon/hse-course-project/cpu-monolith/internal/english"
	"gitlab.com/illfalcon/hse-course-project/cpu-monolith/internal/message"
	"gitlab.com/illfalcon/hse-course-project/cpu-monolith/internal/numbers"
	"gitlab.com/illfalcon/hse-course-project/cpu-monolith/internal/russian"
)

func main() {
	rusService := russian.New()
	engService := english.New()
	numService := numbers.New()

	msgService := message.New(rusService, engService, numService)
	r := mux.NewRouter()
	r.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		msg, err := ioutil.ReadAll(request.Body)
		if err != nil {
			writer.WriteHeader(500)
			writer.Write([]byte("error"))
		}
		writer.WriteHeader(200)
		writer.Write([]byte(msgService.Transform(string(msg))))
	}).Methods(http.MethodPost)
	log.Println("start")
	log.Fatal(http.ListenAndServe(":8080", r))
}

module gitlab.com/illfalcon/hse-course-project/cpu-monolith

go 1.14

require (
	github.com/fiam/gounidecode v0.0.0-20150629112515-8deddbd03fec
	github.com/gorilla/mux v1.7.4
)

package english

import (
	"strings"
	"time"
)

type Service struct {
}

func New() *Service {
	return &Service{}
}

func (s *Service) Transform(words map[int]string) {
	for k, v := range words {
		newWord := strings.ToUpper(v)
		words[k] = newWord
		done := make(chan struct{})

		for i := 0; i < 2; i++ {
			go func() {
				for {
					select {
					case <-done:
						return
					default:
					}
				}
			}()
		}

		time.Sleep(200 * time.Millisecond)
		close(done)
	}
}

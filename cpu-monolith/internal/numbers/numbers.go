package numbers

import (
	"strconv"
	"time"
)

type Service struct {
}

func New() *Service {
	return &Service{}
}

func (s *Service) Transform(words map[int]string) {
	for k, v := range words {
		i, err := strconv.Atoi(v)
		if err != nil {
			words[k] = "ERROR"
			continue
		}

		done := make(chan struct{})

		for i := 0; i < 5; i++ {
			go func() {
				for {
					select {
					case <-done:
						return
					default:
					}
				}
			}()
		}

		time.Sleep(200 * time.Millisecond)
		close(done)

		newWord := strconv.Itoa(i * i)
		words[k] = newWord
	}
}

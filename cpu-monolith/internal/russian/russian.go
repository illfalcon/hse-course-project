package russian

import (
	"time"

	"github.com/fiam/gounidecode/unidecode"
)

type Service struct {
}

func New() *Service {
	return &Service{}
}

func (s *Service) Transform(words map[int]string) {
	for k, v := range words {
		newWord := unidecode.Unidecode(v)
		words[k] = newWord

		done := make(chan struct{})

		for i := 0; i < 5; i++ {
			go func() {
				for {
					select {
					case <-done:
						return
					default:
					}
				}
			}()
		}

		time.Sleep(200 * time.Millisecond)
		close(done)
	}
}

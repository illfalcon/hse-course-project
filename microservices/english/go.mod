module gitlab.com/illfalcon/hse-course-project/microservices/english

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/lib/pq v1.4.0
	github.com/pkg/errors v0.9.1
)

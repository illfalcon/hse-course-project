package rpc

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/pkg/errors"
)

type Client struct {
	httpClient *http.Client
	url        string
}

func NewClient(timeout time.Duration) *Client {
	return &Client{httpClient: &http.Client{
		Timeout: timeout,
	}}
}

func (c *Client) Post(url string, data []byte) ([]byte, error) {
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(data))
	if err != nil {
		return nil, errors.Wrap(err, "error creating request")
	}
	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode == 500 {
		return nil, errors.New("unsuccessful response")
	}
	return ioutil.ReadAll(resp.Body)
}

package transformer

import (
	"encoding/json"

	"gitlab.com/illfalcon/hse-course-project/microservices/gateway/internal/encoding"
)

type client interface {
	Post(url string, body []byte) ([]byte, error)
}

type Service struct {
	client client
	host   string
}

func New(client client, host string) *Service {
	return &Service{
		client: client,
		host:   host,
	}
}

func (s *Service) Transform(m map[int]string) map[int]string {
	reqBody, err := json.Marshal(encoding.IntToStringMap(m))
	if err != nil {
		return errorMap(m, err)
	}
	respBody, err := s.client.Post(s.host, reqBody)
	if err != nil {
		return errorMap(m, err)
	}
	var res map[string]string
	err = json.Unmarshal(respBody, &res)
	if err != nil {
		return errorMap(m, err)
	}
	finalRes, err := encoding.StringToIntMap(res)
	if err != nil {
		return errorMap(m, err)
	}
	return finalRes
}

func errorMap(m map[int]string, err error) map[int]string {
	res := make(map[int]string)
	for k, _ := range m {
		res[k] = err.Error()
	}
	return res
}

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"gitlab.com/illfalcon/hse-course-project/microservices/numbers/internal/postgres"
	"gitlab.com/illfalcon/hse-course-project/microservices/numbers/internal/service"

	"github.com/gorilla/mux"
)

func main() {
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	connStr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", dbHost, dbPort, dbUser, dbPassword, dbName)

	db, err := postgres.NewDB(connStr)
	if err != nil {
		log.Fatalf("cannot connect to db: %s", err)
	}
	defer db.Close()
	numService := service.New(db)

	r := mux.NewRouter()
	r.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		msg, err := ioutil.ReadAll(request.Body)
		if err != nil {
			writer.WriteHeader(500)
			writer.Write([]byte("error"))
		}
		resp, err := numService.Transform(msg)
		if err != nil {
			writer.WriteHeader(500)
			writer.Write([]byte("error"))
		}
		writer.WriteHeader(200)
		writer.Write(resp)
	}).Methods(http.MethodPost)
	log.Println("start")
	log.Fatal(http.ListenAndServe(":8080", r))
}

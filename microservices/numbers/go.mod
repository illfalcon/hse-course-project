module gitlab.com/illfalcon/hse-course-project/microservices/numbers

go 1.14

require (
	github.com/gorilla/mux v1.7.4 // indirect
	github.com/lib/pq v1.4.0
	github.com/pkg/errors v0.9.1
)

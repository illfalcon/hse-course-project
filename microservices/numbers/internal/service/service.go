package service

import (
	"encoding/json"
	"strconv"

	"gitlab.com/illfalcon/hse-course-project/microservices/numbers/internal/weberrors"
)

const tableName = "numbers"

type storage interface {
	QueryRow(table, value string) (string, error)
	InsertRow(table, word, newWord string) error
}

type Service struct {
	storage storage
}

func New(storage storage) *Service {
	return &Service{storage: storage}
}

func (s *Service) Transform(data []byte) ([]byte, error) {
	var words map[string]string
	err := json.Unmarshal(data, &words)
	if err != nil {
		return nil, err
	}
	for k, v := range words {
		res, err := s.storage.QueryRow(tableName, v)
		if err == weberrors.NotFound {
			i, err := strconv.Atoi(v)
			if err != nil {
				words[k] = "ERROR"
				continue
			}
			newWord := strconv.Itoa(i * i)
			words[k] = newWord
			_ = s.storage.InsertRow(tableName, v, newWord)
			continue
		}
		if err != nil {
			words[k] = "ERROR"
			continue
		}
		words[k] = res
	}
	res, err := json.Marshal(words)
	if err != nil {
		return nil, err
	}
	return res, nil
}

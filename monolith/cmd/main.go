package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"gitlab.com/illfalcon/hse-course-project/monolith/internal/english"
	"gitlab.com/illfalcon/hse-course-project/monolith/internal/numbers"
	"gitlab.com/illfalcon/hse-course-project/monolith/internal/postgres"
	"gitlab.com/illfalcon/hse-course-project/monolith/internal/russian"

	"gitlab.com/illfalcon/hse-course-project/monolith/internal/message"

	"github.com/gorilla/mux"
)

func main() {
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbRusName := os.Getenv("DB_RUSSIAN_NAME")
	dbEngName := os.Getenv("DB_ENGLISH_NAME")
	dbNumName := os.Getenv("DB_NUMBERS_NAME")

	connRusStr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", dbHost, dbPort, dbUser, dbPassword, dbRusName)
	connEngStr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", dbHost, dbPort, dbUser, dbPassword, dbEngName)
	connNumStr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", dbHost, dbPort, dbUser, dbPassword, dbNumName)

	dbRus, err := postgres.NewDB(connRusStr)
	if err != nil {
		log.Fatalf("cannot connect to db: %s", err)
	}
	defer dbRus.Close()
	dbEng, err := postgres.NewDB(connEngStr)
	if err != nil {
		log.Fatalf("cannot connect to db: %s", err)
	}
	defer dbEng.Close()
	dbNum, err := postgres.NewDB(connNumStr)
	if err != nil {
		log.Fatalf("cannot connect to db: %s", err)
	}
	defer dbNum.Close()
	rusService := russian.New(dbRus)
	engService := english.New(dbEng)
	numService := numbers.New(dbNum)

	msgService := message.New(rusService, engService, numService)
	r := mux.NewRouter()
	r.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		msg, err := ioutil.ReadAll(request.Body)
		if err != nil {
			writer.WriteHeader(500)
			writer.Write([]byte("error"))
		}
		writer.WriteHeader(200)
		writer.Write([]byte(msgService.Transform(string(msg))))
	}).Methods(http.MethodPost)
	log.Println("start")
	log.Fatal(http.ListenAndServe(":8080", r))
}

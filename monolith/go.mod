module gitlab.com/illfalcon/hse-course-project/monolith

go 1.14

require (
	github.com/fiam/gounidecode v0.0.0-20150629112515-8deddbd03fec
	github.com/gorilla/mux v1.7.4
	github.com/jackc/pgx v3.6.2+incompatible // indirect
	github.com/lib/pq v1.4.0
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20200427165652-729f1e841bcc // indirect
	golang.org/x/text v0.3.2 // indirect
)

package message

import (
	"strings"
	"sync"
)

type transformer interface {
	Transform(map[int]string)
}

func isRussian(s string) bool {
	for _, r := range s {
		if (r < 'а' || r > 'я') && (r < 'А' || r > 'Я') {
			return false
		}
	}
	return true
}

func isEnglish(s string) bool {
	for _, r := range s {
		if (r < 'a' || r > 'z') && (r < 'A' || r > 'Z') {
			return false
		}
	}
	return true
}

func isNumber(s string) bool {
	for _, r := range s {
		if r < '0' || r > '9' {
			return false
		}
	}
	return true
}

type Service struct {
	russian transformer
	english transformer
	numbers transformer
}

func New(russian, english, numbers transformer) *Service {
	return &Service{
		russian: russian,
		english: english,
		numbers: numbers,
	}
}

func (s *Service) Transform(msg string) string {
	russianWords := make(map[int]string)
	englishWords := make(map[int]string)
	numberWords := make(map[int]string)
	invalidWords := make(map[int]string)

	words := strings.Split(msg, " ")

	for i, word := range words {
		if isRussian(word) {
			russianWords[i] = word
			continue
		}
		if isEnglish(word) {
			englishWords[i] = word
			continue
		}
		if isNumber(word) {
			numberWords[i] = word
			continue
		}
		invalidWords[i] = word
	}
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		s.english.Transform(englishWords)
	}()
	wg.Add(1)
	go func() {
		defer wg.Done()
		s.russian.Transform(russianWords)
	}()
	wg.Add(1)
	go func() {
		defer wg.Done()
		s.numbers.Transform(numberWords)
	}()
	wg.Wait()
	for k, v := range russianWords {
		words[k] = v
	}
	for k, v := range englishWords {
		words[k] = v
	}
	for k, v := range numberWords {
		words[k] = v
	}
	for k, _ := range invalidWords {
		words[k] = "ERROR"
	}
	return strings.Join(words, " ")
}

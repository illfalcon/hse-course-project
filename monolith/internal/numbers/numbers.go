package numbers

import (
	"strconv"

	"gitlab.com/illfalcon/hse-course-project/monolith/internal/weberrors"
)

const tableName = "numbers"

type storage interface {
	QueryRow(table, value string) (string, error)
	InsertRow(table, word, newWord string) error
}

type Service struct {
	storage storage
}

func New(storage storage) *Service {
	return &Service{storage: storage}
}

func (s *Service) Transform(words map[int]string) {
	for k, v := range words {
		res, err := s.storage.QueryRow(tableName, v)
		if err == weberrors.NotFound {
			i, err := strconv.Atoi(v)
			if err != nil {
				words[k] = "ERROR"
				continue
			}
			newWord := strconv.Itoa(i * i)
			words[k] = newWord
			_ = s.storage.InsertRow(tableName, v, newWord)
			continue
		}
		if err != nil {
			words[k] = "ERROR"
			continue
		}
		words[k] = res
	}
}

package postgres

import (
	"database/sql"
	"fmt"
	"log"

	"gitlab.com/illfalcon/hse-course-project/monolith/internal/weberrors"

	"github.com/pkg/errors"

	_ "github.com/lib/pq"
)

type DB struct {
	db *sql.DB
}

func NewDB(conn string) (*DB, error) {
	db, err := sql.Open("postgres", conn)
	if err != nil {
		return nil, errors.Wrap(err, "error connecting to db")
	}
	log.Print("successfully connected to db")
	stmt := `create table if not exists russian (
		id serial,
		word text,
		new_word text
	);`
	_, err = db.Exec(stmt)
	if err != nil {
		return nil, errors.Wrap(err, "error creating table 'russian'")
	}
	stmt = `create table if not exists english (
		id serial,
		word text,
		new_word text
	);`
	_, err = db.Exec(stmt)
	if err != nil {
		return nil, errors.Wrap(err, "error creating table 'english'")
	}
	stmt = `create table if not exists numbers (
		id serial,
		word text,
		new_word text
	);`
	_, err = db.Exec(stmt)
	if err != nil {
		return nil, errors.Wrap(err, "error creating table 'numbers'")
	}
	return &DB{db: db}, nil
}

func (db *DB) QueryRow(tableName, value string) (string, error) {
	stmt := fmt.Sprintf(`select new_word from %s where word = $1`, tableName)
	row := db.db.QueryRow(stmt, value)
	var result string
	err := row.Scan(&result)
	if err == sql.ErrNoRows {
		return "", weberrors.NotFound
	}
	if err != nil {
		log.Print(err)
		return "", err
	}
	return result, nil
}

func (db *DB) InsertRow(tableName, word, newWord string) error {
	stmt := fmt.Sprintf(`insert into %s (word, new_word) values ($1, $2)`, tableName)
	_, err := db.db.Exec(stmt, word, newWord)
	if err != nil {
		log.Print(err)
		return err
	}
	return nil
}

func (db *DB) Close() error {
	return db.db.Close()
}
